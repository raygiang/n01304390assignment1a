﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Assignment1a.aspx.cs" Inherits="Assignment1a.Assignment1a" %>

<!DOCTYPE html>
<%-- Assignment 1a
     Raymond Giang
     n01304390 --%>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Local Dog Services</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Local Dog Services</h1>
            
            <asp:ValidationSummary id="validSummary" Runat="server" />

            <asp:label runat="server" AssociatedControlID="clientName">Client Name: </asp:label>
            <asp:TextBox runat="server" ID="clientName" placeholder="Your Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Your Name" ControlToValidate="clientName" ID="nameValidator"></asp:RequiredFieldValidator>
            <br />

            <asp:label runat="server" AssociatedControlID="clientPhone">Phone Number: </asp:label>
            <asp:TextBox runat="server" ID="clientPhone" placeholder="e.g. 905-111-2222"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter a Phone Number" ControlToValidate="clientPhone" ID="phoneValidator"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexPhone" runat="server" ValidationExpression="^(\+\d{1,2}\s)?\(?\d{3}\)?[\s.-]\d{3}[\s.-]\d{4}$" ControlToValidate="clientPhone" ErrorMessage="Invalid Phone Number"></asp:RegularExpressionValidator>
            <br />

            <asp:Label runat="server" AssociatedControlID="clientEmail">Email Address: </asp:Label>
            <asp:TextBox runat="server" ID="clientEmail" placeholder="Your Email Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Your Email Address" ControlToValidate="clientEmail" ID="emailValidator"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="regexEmail" runat="server" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" ControlToValidate="clientEmail" ErrorMessage="Invalid Email Address Format"></asp:RegularExpressionValidator>
            <br />

            <asp:Label runat="server" AssociatedControlID="streetAddress">Street Address: </asp:Label>
            <asp:TextBox runat="server" ID="streetAddress" placeholder="Your Street Address"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Your Street Address" ControlToValidate="streetAddress" ID="addressValidator"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" ControlToValidate="streetAddress" Type="String" Operator="NotEqual" ValueToCompare="123 Fake Street" ErrorMessage="This is not a Valid Street Address" ID="realStreetValidator"></asp:CompareValidator>
            <br />
            
            <asp:Label runat="server" AssociatedControlID="postalCode">Postal Code: </asp:Label>
            <asp:TextBox runat="server" ID="postalCode" placeholder="Your Postal Code"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Your Postal Code" ControlToValidate="postalCode" ID="postalValidator"></asp:RequiredFieldValidator>
            <br />
            <br />

            <asp:Label runat="server" AssociatedControlID="dogName">Dog Name: </asp:Label>
            <asp:TextBox runat="server" ID="dogName" placeholder="Your Dog's Name"></asp:TextBox>
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please Enter Your Dog's Name" ControlToValidate="dogName" ID="dogNameValidator"></asp:RequiredFieldValidator>
            <br />

            <asp:Label runat="server">What services do you need?</asp:Label>
            <div id="services" runat="server">
                <asp:CheckBox runat="server" ID="dogService1" Text="Dog Walking" />
                <asp:CheckBox runat="server" ID="dogService2" Text="Dog Bathing" />
                <asp:CheckBox runat="server" ID="dogService3" Text="Dog Sitting" />
            </div>

            <asp:Label runat="server" AssociatedControlID="walkLength">How long would you like your dog walked? (if applicable): </asp:Label>
            <asp:TextBox runat="server" ID="walkLength" placeholder="30 to 120 mins"></asp:TextBox>
            <asp:RangeValidator runat="server" ControlToValidate="walkLength" Type="Integer" MinimumValue="30" MaximumValue="120" ErrorMessage="Enter a valid time (30 - 120)"></asp:RangeValidator>
            <br />
            <br />

            <fieldset>
                <legend>How would you like to leave your dog with us?</legend>
                <asp:RadioButton runat="server" Text="I would like to drop my dog off" GroupName="receive"/>
                <asp:RadioButton runat="server" Text="I would like for you to pick up my dog from my address" GroupName="receive"/>
                <br />

                <asp:Label runat="server" AssociatedControlID="receiveTime">What time would you like for pick up/drop off: </asp:Label>
                <asp:DropDownList runat="server" ID="receiveTime">
                    <asp:ListItem Value="12:00" Text="12:00"></asp:ListItem>
                    <asp:ListItem Value="13:00" Text="13:00"></asp:ListItem>
                    <asp:ListItem Value="14:00" Text="14:00"></asp:ListItem>
                    <asp:ListItem Value="14:00" Text="15:00"></asp:ListItem>
                    <asp:ListItem Value="14:00" Text="16:00"></asp:ListItem>
                    <asp:ListItem Value="14:00" Text="17:00"></asp:ListItem>
                    <asp:ListItem Value="14:00" Text="18:00"></asp:ListItem>
            </asp:DropDownList>
            </fieldset>

            <fieldset>
                <legend>How would you like to take back your dog?</legend>
                <asp:RadioButton runat="server" Text="I would like my dog to be dropped off" GroupName="return"/>
                <asp:RadioButton runat="server" Text="I would like to come pick up my dog" GroupName="return"/>
                <br />

                <asp:Label runat="server" AssociatedControlID="returnTime">What time would you like for pick up/drop off: </asp:Label>
                <asp:DropDownList runat="server" ID="returnTime">
                    <asp:ListItem Value="12:00" Text="13:00"></asp:ListItem>
                    <asp:ListItem Value="13:00" Text="14:00"></asp:ListItem>
                    <asp:ListItem Value="14:00" Text="15:00"></asp:ListItem>
                    <asp:ListItem Value="14:00" Text="16:00"></asp:ListItem>
                    <asp:ListItem Value="14:00" Text="17:00"></asp:ListItem>
                    <asp:ListItem Value="14:00" Text="18:00"></asp:ListItem>
                    <asp:ListItem Value="14:00" Text="19:00"></asp:ListItem>
                    <asp:ListItem Value="14:00" Text="20:00"></asp:ListItem>
            </asp:DropDownList>
            </fieldset>
            <br />

            <asp:CheckBox runat="server" ID="emailSubscribe" Text="I would like to receive emails from Local Dog Services" checked="true"/>
            <br />

            <br />
                <asp:Button runat="server" ID="submitButton" Text="Submit"/>
            <br />
        </div>
    </form>
</body>
</html>
